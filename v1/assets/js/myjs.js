$(document).ready(function() {
	new WOW().init();

    $("table.responsive").each(function() {
        var headers = [];
        $(this).find("th").each(function () {
            headers.push($(this).text().trim());
        });

        // Generate CSS string
        var css_str = "<style>@media only screen and (max-width: 760px), (min-device-width: 768px) and (max-device-width: 1024px) { ";
        var header, num;
        for (var i in headers) {
          header = headers[i].replace(/([\\"])/g, "\\$1").replace(/\n/g, " ");
          num = parseInt(i,10)+1;
          css_str += "table.responsive td:nth-of-type("+num+"):before { content: \""+header+"\"; }" + "\n";
        }
        css_str += " }</style>";
        $(css_str).appendTo("head");
    });

});







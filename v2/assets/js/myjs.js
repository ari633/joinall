$(document).ready(function() {
	new WOW().init();

    $("table.responsive").each(function() {
        var headers = [];
        $(this).find("th").each(function () {
            headers.push($(this).text().trim());
        });

        // Generate CSS string
        var css_str = "<style>@media only screen and (max-width: 760px), (min-device-width: 768px) and (max-device-width: 1024px) { ";
        var header, num;
        for (var i in headers) {
          header = headers[i].replace(/([\\"])/g, "\\$1").replace(/\n/g, " ");
          num = parseInt(i,10)+1;
          css_str += "table.responsive td:nth-of-type("+num+"):before { content: \""+header+"\"; }" + "\n";
        }
        css_str += " }</style>";
        $(css_str).appendTo("head");
    });

	// Rotator carrousel
	$('.slider').each(function() {
		var $this = $(this),
			$items = $this.children('li'),
			current = Math.round($items.length / 2),
			$current = $($items[current]),
			interval;

		$items.hide();

		$items.find('a.hitarea').click(function(event) {
			var $this = $(this),
				$parent = $this.parent(),
				$new;

			if ($parent.hasClass('active')) {
				return true;
			}

			event.preventDefault();

			if ($parent.next().hasClass('active')) {
				// clicked on prev
				$parent.next().next().hide();
				$parent.next().removeClass('active');
				$parent.addClass('active');

				if ($parent.prev().length === 0) {
					// rearrange
					$parent.siblings().last().prependTo($parent.parent()).show();
				} else {
					$parent.prev().show();
				}
			} else {
				// clicked on next
				$parent.prev().prev().hide();
				$parent.prev().removeClass('active');
				$parent.addClass('active');

				if ($parent.next().length === 0) {
					// rearrange
					$parent.siblings().first().appendTo($parent.parent()).show();
				} else {
					$parent.next().show();
				}
			}
		});

		$current.addClass('active').show();
                
                var mq = window.matchMedia( "(min-width: 500px)" );
                if (mq.matches) {
                        $current.prev().show();
                        $current.next().show();
                
                        interval = setInterval(function() {
                                $('.slider li.active + li a.hitarea').trigger('click');
                        }, 3000);
                
                 }else{
                        $current.prev().hide();
                        $current.next().hide();                     
                 }

                
	}).show();

});






